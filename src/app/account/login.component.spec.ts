import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AppRoutingModule } from './../app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';

import { LoginComponent } from './login.component';

import { AccountService, AlertService } from '@app/_services';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  const routerClientStub: jasmine.SpyObj<Router> = jasmine.createSpyObj(
    'router',
    ['post']
  );
  const httpClientStub: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj(
    'http',
    ['post']
  );

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ], 
      imports: [ 
        ReactiveFormsModule, 
        HttpClientTestingModule, 
        RouterModule.forRoot([])
      ],
      providers: [
        { provide: ActivatedRoute, useValue: routerClientStub },
        AccountService
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
  fixture = TestBed.createComponent(LoginComponent);
  component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
