﻿import { Component } from '@angular/core';

import { AccountService, FileService } from './_services';
import { User } from './_models';

@Component({ selector: 'app', templateUrl: 'app.component.html' })
export class AppComponent {
  user: User;

  constructor(
    private fileService: FileService,
    private accountService: AccountService) {
      this.accountService.user.subscribe(x => this.user = x);
  }

  download():void {
    const resumeFile = './assets/CV_DYLAN_LOPEZ_ENG-pdf';
    this.fileService.getReport(resumeFile).subscribe(response => {
      console.log("fileService response: ", response);
    });
  }

  logout() {
    this.accountService.logout();
  }
}
