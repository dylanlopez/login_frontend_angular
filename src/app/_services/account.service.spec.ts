import { AccountService } from './account.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { of } from 'rxjs';

describe('AccountService', () => {
  it('should perform a post to /login with email and password', () => {
    const email = 'alex@outlook.com';
    const password = '123456';
    let userRequest = {
      Email: email,
      Password: password
    }
    const routerClientStub: jasmine.SpyObj<Router> = jasmine.createSpyObj(
      'router',
      ['post']
    );
    const httpClientStub: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj(
      'http',
      ['post']
    );
    const accountService = new AccountService(routerClientStub, httpClientStub);
    httpClientStub.post.and.returnValue(of());
    const response = accountService.login(email, password);
    console.log(response);
    expect(httpClientStub.post).toHaveBeenCalledWith(`${environment.apiUrl}/api/Login/loginUser`, userRequest);
  });

  it('should not perform a post to /login with email and password', () => {
    const email = 'alex@outlook.com';
    const password = '654321';
    let userRequest = {
      Email: email,
      Password: password
    }
    const routerClientStub: jasmine.SpyObj<Router> = jasmine.createSpyObj(
      'router',
      ['post']
    );
    const httpClientStub: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj(
      'http',
      ['post']
    );
    const accountService = new AccountService(routerClientStub, httpClientStub);
    httpClientStub.post.and.returnValue(of());
    const response = accountService.login(email, password);
    console.log(response);
    expect(httpClientStub.post).toHaveBeenCalledWith(`${environment.apiUrl}/api/Login/loginUser`, userRequest);
  });
});
