﻿import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '@environments/environment';
import { User, UserResponse } from '@app/_models';

@Injectable({ providedIn: 'root' })
export class AccountService {
  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>;

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
    this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
    this.user = this.userSubject.asObservable();
  }

  public get userValue(): User {
    return this.userSubject.value;
  }

  login(email, password) {
    // console.log("login");
    let userRequest = {
      Email: email,
      Password: password
    }
    // console.log(" userRequest: ", userRequest);
    return this.http.post<UserResponse>(`${environment.apiUrl}/api/Login/loginUser`, userRequest)
      .pipe(map(userResponse => {
        // console.log(" user: ", user);
        if (userResponse.data.length > 0){
          let user: User = userResponse.data[0];
          user.isActive = false;
          if (user.active == 1){
            user.isActive = true;
          }
          // console.log(" user: ", user);
          localStorage.setItem('user', JSON.stringify(user));
          this.userSubject.next(user);
          return user;
        }
      }));
  }

  logout() {
    localStorage.removeItem('user');
    this.userSubject.next(null);
    this.router.navigate(['/account/login']);
  }

  register(email, password) {
    // console.log("register");
    let userRequest = {
      Email: email,
      Password: password
    }
    // console.log(" userRequest: ", userRequest);
    return this.http.post(`${environment.apiUrl}/api/Login/createUser`, userRequest);
  }
}
