import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class FileService {
  constructor(private http: HttpClient) {
  }

  getReport(url: string): Observable<any> {
    return this.http.get(url, {
      responseType: 'blob'
    });
  }
}
