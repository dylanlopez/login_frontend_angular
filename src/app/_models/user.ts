﻿export class User {
  id: number;
  email: string;
  password: string;
  passwordD: string;
  active: number;
  isActive: boolean;
  token: string;
}
