# login_frontend_angular

This project can been open with Visual Studio Code in Windows or Linux S.O. But must be installed NPM, ANGULAR CLI, the project is in ([my GitLab account](https://gitlab.com/dylanlopez/login_frontend_angular)).

After clone, the repository opens with Visual Studio Code and executes `npm install` to download the node dependencies.

For execute, run the command `ng serve`. Is very important that this project references Backend API (Net Core) and these should be running before.
